<!-- 

4. Create another function named getLetterGrade() that uses conditional statements to output a letter representation of a given numerical grade:
    - A+ (98 to 100)
    - A (95 to 97)
    - A- (92 to 94)
    - B+ (89 to 91)
    - B (86 to 88)
    - B- (83 to 85)
    - C+ (80 to 82)
    - C (77 to 79)
    - C- (75 to 76)
    - D (75 below)
5. Include the code.php in the index.html and invoke the created methods. -->
<?php

function getFullAddress($country, $city, $province, $specificAdress){
    return "$specificAdress, $city, $province, $country";
}

function getLetterGrade($grade){
    if($grade >=98 && $grade <=100){
        return "$grade is equivalent to A+";
    } else if ($grade >=95){
        return "$grade is equivalent to A";
    } else if ($grade >=92){
        return "$grade is equivalent to A-";
    } else if ($grade >=89){
        return "$grade is equivalent to B+";
    } else if ($grade >=86){
        return "$grade is equivalent to B";
    } else if ($grade >=83){
        return "$grade is equivalent to B-";
    } else if ($grade >=80){
        return "$grade is equivalent to C+";
    } else if ($grade >=77){
        return "$grade is equivalent to C";
    } else if ($grade >=75){
        return "$grade is equivalent to C-";
    } else {
        return "$grade is equivalent to D";
    }
}