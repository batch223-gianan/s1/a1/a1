<?php require_once './code.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Full Adress</h1>
    <p><?php echo getFullAddress('Philippines', 'Quezcon City', 'Metro Manila', '3F Caswyn Bldg.'); ?></p>
    <p><?php echo getFullAddress('Philippines', 'Makati City', 'Buendia Avenue', '3F Enzo Bldg.'); ?></p>

    <h1>Letter-Based Grading</h1>
    <p><?php echo getLetterGrade(87); ?></p>
    <p><?php echo getLetterGrade(94); ?></p>
    <p><?php echo getLetterGrade(74); ?></p>
</body>
</html>